import os
import subprocess
from config import Config
from folder import Folder


class AgentFactory:
    counter = 0

    @staticmethod
    def get_agent(fs='real'):
        """
        Get agent by filespace key
        :param fs: key
        :return: Agent
        """

        AgentFactory.counter += 1

        return Agent(
            AgentFactory.counter,
            Config.fs[fs],
            Config.password,
            os.path.join(Config.mount_point, str(AgentFactory.counter))
        )


class Agent:
    def __init__(self, instance, fs, password, mount_point):
        self.instance = instance
        self.fs = fs
        self.password = password
        self.mount_point = mount_point
        self.daemon = None
        self.folder = Folder(mount_point)

    def status(self):
        result = {}
        lines = subprocess.check_output(["lucid", "--instance", str(self.instance), "status"]).decode().split('\n')

        for line in lines:
            parts = line.split(': ')
            if len(parts) == 2:
                result[parts[0]] = parts[1]

        return result

    def link(self):
        try:
            result = subprocess.check_output([
                "lucid",
                "--instance", str(self.instance),
                "link",
                "--fs", self.fs,
                "--password", self.password,
                "--mount-point", self.mount_point
            ])
            return dict(code=0, message=result.decode().strip())
        except subprocess.CalledProcessError as e:
            return dict(code=e.returncode, message=e.output.decode().strip())

    def unlink(self):
        try:
            result = subprocess.check_output([
                "lucid",
                "--instance", str(self.instance),
                "unlink",
            ])
            return dict(code=0, message=result.decode().strip())
        except subprocess.CalledProcessError as e:
            return dict(code=e.returncode, message=e.output.decode().strip())

    def sync(self):
        subprocess.call(['lucid', '--instance', str(self.instance), 'sync'])

    def start_daemon(self):
        self.daemon = subprocess.Popen(['lucid', '--instance', str(self.instance), 'daemon', '&'])

    def stop_daemon(self):
        if self.daemon is not None:
            self.daemon.terminate()
