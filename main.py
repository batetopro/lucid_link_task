from test import *
from logger import Logger


queue = [
    TestLink(),
    TestDisconnected(),
    TestOverLink(),
    TestWrongCredentials(),
    TestWriteParallel(),
    TestWritePersistent(),
    TestDownloadPersistent(),
    TestDownloadParallel(),
    TestMakeFolderPersistent(),
    TestMakeFolderParallel(),
]


logger = Logger()

for test in queue:
    try:
        logger.start(test.__class__.__name__)
        test.test()
        logger.finish()
    except Exception as e:
        logger.fail()
