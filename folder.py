import os
import hashlib
import urllib.request


class Folder:
    def __init__(self, mount_point):
        self.mount_point = mount_point

    def get(self, name):
        return os.path.join(self.mount_point, name)

    def exists(self, name=None):
        if not name:
            return os.path.exists(self.mount_point)

        return os.path.exists(self.get(name))

    def is_empty(self):
        if not self.exists():
            return True

        return os.listdir(self.mount_point).__len__() == 0

    def clean(self):
        if self.exists() and self.is_empty():
            os.removedirs(self.mount_point)

    def download(self, url, name):
        filename = self.get(name)
        urllib.request.urlretrieve(url, filename)

    def get_checksum(self, name):
        if not self.exists(name):
            return None

        filename = self.get(name)

        hash_md5 = hashlib.md5()
        with open(filename, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def delete(self, name):
        if not self.exists(name):
            return

        filename = self.get(name)
        os.unlink(filename)
