import sqlite3
import traceback


class Logger:
    def __init__(self):
        self.conn = sqlite3.connect('test.db')
        self.cursor = self.conn.cursor()
        self.id = None
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS log 
                (
                    id integer primary key,
                    test text,
                    status text,
                    start_time datetime,
                    end_time datetime,
                    error text
                );
            ''')

    def close(self):
        self.conn.close()

    def start(self, name):
        sql = "INSERT INTO log(test, status, start_time) VALUES (?, 'IN PROGRESS', datetime('now'))"
        self.cursor.execute(sql, (name, ))
        self.id = self.cursor.lastrowid
        self.conn.commit()

    def finish(self):
        sql = "UPDATE log SET status = 'COMPLETED', end_time = datetime('now') WHERE id = ?"
        self.cursor.execute(sql, (self.id, ))
        self.id = None
        self.conn.commit()

    def fail(self):
        tb = traceback.format_exc()
        sql = "UPDATE log SET status = 'FAILED', end_time = datetime('now'), error = ? WHERE id = ?"
        self.cursor.execute(sql, (tb, self.id, ))
        self.id = None
        self.conn.commit()