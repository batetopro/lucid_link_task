# Lucid Link tests #

### What is this repository for? ###

* This repository contains test cases for the Lucid Link CLI commands - link and unlink. 
  The cases are documented in the file ``scenario.pdf``
* Version: 1.0
* [Lucid Link](https://www.lucidlink.com/)

### How do I get set up? ###



#### Dependencies ####
The tests require python3 installed on the machine.


#### Configuration ####
Get a working LucidLink filespace. 
Create one by using the [dashboard](https://www.lucidlink.com/webportal/dashboard).

To set up, the configuration file should be filed up.
Copy `config.ini.py` and paste it as `config.py`. Then fill the sections:

* fs['real'] = the configured filespace
* fs['wrong'] =  anything, which is not a filespace
* password = the password, with which the filespace was 
* mount_point = existing folder at the file system, where the filespace is being linked 


##### How to run tests ####

To run the tests, run this command:

```bash
python main.py
```

To see how the tests are being run, use the SQLite database `test.db`

### Who do I talk to? ###

* Repo owner: hristo.i.georgiev@gmail.com
