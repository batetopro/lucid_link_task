import time
import os
import unittest
from agent import AgentFactory
from config import Config


class TestWrongCredentials(unittest.TestCase):
    def test(self):
        agent = AgentFactory.get_agent('wrong')

        agent.start_daemon()
        time.sleep(2)

        check = agent.link()
        self.assertEqual(check['code'], 75)
        self.assertEqual(check['message'],
                         'Request failed with: Bad Request\nThere is currently no active filespace registered by this name.')

        agent.stop_daemon()
        time.sleep(2)


class TestDisconnected(unittest.TestCase):
    def test(self):
        agent = AgentFactory.get_agent('real')

        self.assertTrue(agent.folder.is_empty())

        check = agent.link()
        self.assertEqual(check['code'], 75)
        self.assertEqual(check['message'], 'Connection refused')

        check = agent.unlink()
        self.assertEqual(check['code'], 75)
        self.assertEqual(check['message'], 'Connection refused')


class TestLink(unittest.TestCase):
    def test(self):
        agent = AgentFactory.get_agent('real')

        agent.start_daemon()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')

        check = agent.link()
        self.assertEqual(check['code'], 0)
        self.assertEqual(check['message'], 'Linked to filespace {0}.'.format(Config.fs['real']))
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        check = agent.unlink()
        self.assertEqual(check['code'], 0)
        self.assertEqual(check['message'], 'Unlinked from the filespace.')

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')

        agent.stop_daemon()
        time.sleep(2)


class TestOverLink(unittest.TestCase):
    def test(self):
        agent = AgentFactory.get_agent('real')
        agent2 = AgentFactory.get_agent('real')

        agent2.mount_point = agent.mount_point

        agent.start_daemon()
        agent2.start_daemon()
        time.sleep(2)

        agent.link()
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        time.sleep(10)

        check = agent2.link()

        self.assertEqual(agent2.status()['Filespace object store state'], 'connected')
        self.assertEqual(check['code'], 0)
        self.assertEqual(check['message'], 'Linked to filespace {0}.'.format(Config.fs['real']))

        agent.stop_daemon()
        agent2.stop_daemon()
        time.sleep(2)


class TestDownloadPersistent(unittest.TestCase):
    DOWNLOAD_URL = 'https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg'
    FILENAME = 'Python-logo-notext.svg'

    def test(self):
        agent = AgentFactory.get_agent('real')

        agent.start_daemon()
        time.sleep(2)

        agent.link()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent.folder.exists(self.FILENAME))
        agent.folder.download(self.DOWNLOAD_URL, self.FILENAME)
        self.assertTrue(agent.folder.exists(self.FILENAME))

        agent.unlink()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')
        self.assertFalse(agent.folder.exists(self.FILENAME))

        agent.link()
        time.sleep(2)
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertTrue(agent.folder.exists(self.FILENAME))

        agent.folder.delete(self.FILENAME)
        self.assertFalse(agent.folder.exists(self.FILENAME))

        agent.unlink()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')

        agent.link()
        time.sleep(2)
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent.folder.exists(self.FILENAME))

        agent.stop_daemon()
        time.sleep(2)


class TestDownloadParallel(unittest.TestCase):
    DOWNLOAD_URL = 'https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg'
    FILENAME = 'Python-logo-notext.svg'

    def test(self):
        agent = AgentFactory.get_agent('real')
        agent2 = AgentFactory.get_agent('real')

        agent.start_daemon()
        agent2.start_daemon()
        time.sleep(2)

        agent.link()
        agent2.link()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'connected')
        self.assertEqual(agent2.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent2.folder.exists(self.FILENAME))

        agent.folder.download(self.DOWNLOAD_URL, self.FILENAME)

        agent.sync()
        agent2.sync()
        time.sleep(2)

        self.assertTrue(agent2.folder.exists(self.FILENAME))
        self.assertEqual(agent.folder.get_checksum(self.FILENAME), agent2.folder.get_checksum(self.FILENAME))

        agent.folder.delete(self.FILENAME)

        agent.sync()
        agent2.sync()
        time.sleep(2)

        self.assertFalse(agent2.folder.exists(self.FILENAME))

        agent.stop_daemon()
        agent2.stop_daemon()
        time.sleep(2)


class TestWritePersistent(unittest.TestCase):
    MESSAGE = 'Hello world'
    FILENAME = 'message.txt'

    def test(self):
        agent = AgentFactory.get_agent('real')

        agent.start_daemon()
        time.sleep(2)

        agent.link()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'connected')
        self.assertFalse(agent.folder.exists(self.FILENAME))

        with open(agent.folder.get(self.FILENAME), 'w', encoding='utf8') as fp:
            fp.write(self.MESSAGE)

        self.assertTrue(agent.folder.exists(self.FILENAME))

        agent.unlink()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')
        self.assertFalse(agent.folder.exists(self.FILENAME))

        agent.link()
        time.sleep(2)
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertTrue(agent.folder.exists(self.FILENAME))

        with open(agent.folder.get(self.FILENAME), 'r', encoding='utf8') as fp:
            self.assertEqual(fp.read(), self.MESSAGE)

        agent.folder.delete(self.FILENAME)
        self.assertFalse(agent.folder.exists(self.FILENAME))

        agent.unlink()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')

        agent.link()
        time.sleep(2)
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent.folder.exists(self.FILENAME))

        agent.stop_daemon()
        time.sleep(2)


class TestWriteParallel(unittest.TestCase):
    MESSAGE = 'Hello world'
    FILENAME = 'message.txt'

    def test(self):
        agent = AgentFactory.get_agent('real')
        agent2 = AgentFactory.get_agent('real')

        agent.start_daemon()
        agent2.start_daemon()
        time.sleep(2)

        agent.link()
        agent2.link()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'connected')
        self.assertEqual(agent2.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent2.folder.exists(self.FILENAME))

        with open(agent.folder.get(self.FILENAME), 'w', encoding='utf8') as fp:
            fp.write(self.MESSAGE)

        agent.sync()
        agent2.sync()
        time.sleep(2)

        self.assertTrue(agent2.folder.exists(self.FILENAME))

        with open(agent.folder.get(self.FILENAME), 'r', encoding='utf8') as fp:
            self.assertEqual(fp.read(), self.MESSAGE)

        agent.folder.delete(self.FILENAME)

        agent.sync()
        agent2.sync()
        time.sleep(2)

        self.assertFalse(agent2.folder.exists(self.FILENAME))

        agent.stop_daemon()
        agent2.stop_daemon()
        time.sleep(2)


class TestMakeFolderPersistent(unittest.TestCase):
    folder = 'folder'

    def test(self):
        agent = AgentFactory.get_agent('real')

        agent.start_daemon()
        time.sleep(2)

        agent.link()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'connected')
        self.assertFalse(agent.folder.exists(self.folder))

        os.makedirs(agent.folder.get(self.folder))

        self.assertTrue(agent.folder.exists(self.folder))

        agent.unlink()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')
        self.assertFalse(agent.folder.exists(self.folder))

        agent.link()
        time.sleep(2)
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertTrue(agent.folder.exists(self.folder))

        os.removedirs(agent.folder.get(self.folder))

        self.assertFalse(agent.folder.exists(self.folder))

        agent.unlink()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'deactivated')

        agent.link()
        time.sleep(2)
        self.assertEqual(agent.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent.folder.exists(self.folder))

        agent.stop_daemon()
        time.sleep(2)


class TestMakeFolderParallel(unittest.TestCase):
    folder = 'folder'

    def test(self):
        agent = AgentFactory.get_agent('real')
        agent2 = AgentFactory.get_agent('real')

        agent.start_daemon()
        agent2.start_daemon()
        time.sleep(2)

        agent.link()
        agent2.link()
        time.sleep(2)

        self.assertEqual(agent.status()['Filespace object store state'], 'connected')
        self.assertEqual(agent2.status()['Filespace object store state'], 'connected')

        self.assertFalse(agent2.folder.exists(self.folder))

        os.makedirs(agent.folder.get(self.folder))

        agent.sync()
        agent2.sync()
        time.sleep(2)

        self.assertTrue(agent2.folder.exists(self.folder))

        os.removedirs(agent.folder.get(self.folder))

        agent.sync()
        agent2.sync()
        time.sleep(2)

        self.assertFalse(agent2.folder.exists(self.folder))

        agent.stop_daemon()
        agent2.stop_daemon()
        time.sleep(2)


if __name__ == "__main__":
    unittest.main()

